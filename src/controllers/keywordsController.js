const {
    selectKeywordById,
    selectKeywordUserPair,
    updateKeywordValueForUser,
    insertKeywordValueForUser,
    selectInterestedKeys, selectUsedKeywords, selectWorkKeywords
} = require('../services/keywordsService');

async function findKeywordByID(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await selectKeywordById(id);
        return await res.status(200).json({result: result.rows[0]})
    } catch (err){
        next(err);
    }
}

async function changeKeywordValueForUser(req, res, next){
    try {
        const {user_id, keyword_id, value} = req.body;
        const pair = await selectKeywordUserPair(user_id, keyword_id);
        console.log('changeKeywordValueForUser', pair.rows)
        if (pair.rows.length > 0){
            await updateKeywordValueForUser(user_id, keyword_id, value);
        } else {
            await insertKeywordValueForUser(user_id, keyword_id, value);
        }
        return await res.status(200).json(`Keyword value has been updated for user ${user_id}`)
    } catch (err){
        next(err);
    }
}

async function findInterestedKeywords(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await selectInterestedKeys(id);
        if (result.rows.length < 1) {
            const used = await selectUsedKeywords(id);
            for (const obj of used.rows){
                for (const key of obj.keywords){
                    const pair = await selectKeywordUserPair(id, key);
                    if (pair.rows.length > 0){
                        await updateKeywordValueForUser(id, key, 50);
                    } else {
                        await insertKeywordValueForUser(id, key, 50);
                    }
                }
            }
            const resultAfterCalc = await selectInterestedKeys(id);
            return await res.status(200).json({result: resultAfterCalc.rows});
        } else {
            return await res.status(200).json({result: result.rows});
        }
    } catch (err){
        next(err);
    }
}

async function findProjectKeywords(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await selectWorkKeywords(id);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

module.exports = {
    findKeywordByID,
    changeKeywordValueForUser,
    findInterestedKeywords,
    findProjectKeywords
};