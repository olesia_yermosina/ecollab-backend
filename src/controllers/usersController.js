const {
    selectUsersByKeywords,
} = require('../services/usersService');
const {selectNegativeKeys} = require("../services/keywordsService");

async function getUsersByKeywords(req, res, next){
    try {
        const {id, keywords} = req.params;
        console.log(id, keywords);
        const negObjArr = await selectNegativeKeys(id);
        const negArr = [];
        for (const obj of negObjArr.rows) {
            negArr.push(+obj.id);
        }
        console.log(negArr)
        const result = await selectUsersByKeywords(id, keywords, negArr);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

module.exports = {
    getUsersByKeywords
};
