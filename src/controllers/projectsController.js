const {
    selectProjectsByTitle,
    selectProjectById, selectProjectsByKeywords,
    updateRatingValueForProject, selectProjectUserPair,
    updateRatingValueForUserProject, insertRatingValueForUserProject,
    selectProjectsWithRating, selectProjectsByAuthor,
    countProjectsByAuthor, countProjectsByTitle,
    countProjectsByOneKeyword, selectProjectsByOneKeyword,
    selectFavoriteProjectsForUser, countFavoriteProjectsForUser, selectThreeMoreTimesRiddenProjects,
    countThreeMoreTimesRiddenProjects, updateRiddenForUserProject,
} = require('../services/projectsService');
const {selectNegativeKeys} = require("../services/keywordsService");

async function findProjectsByTitle(req, res, next){
    try {
        const {str, index} = req.params;
        console.log(str, index);
        const result = await selectProjectsByTitle(str, index);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

async function countNumberProjectsByTitle(req, res, next){
    try {
        const {str} = req.params;
        console.log(str);
        const result = await countProjectsByTitle(str);
        return await res.status(200).json({result: result.rows[0]})
    } catch (err){
        next(err);
    }
}

async function findProjectsByAuthor(req, res, next){
    try {
        const {id, index} = req.params;
        console.log(id, index);
        const result = await selectProjectsByAuthor(id, index);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

async function countNumberProjectsByAuthor(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await countProjectsByAuthor(id);
        return await res.status(200).json({result: result.rows[0]})
    } catch (err){
        next(err);
    }
}

async function findProjectsByOneKeyword(req, res, next){
    try {
        const {id, index} = req.params;
        console.log(id, index);
        const result = await selectProjectsByOneKeyword(id, index);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

async function countNumberProjectsByOneKeyword(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await countProjectsByOneKeyword(id);
        return await res.status(200).json({result: result.rows[0]})
    } catch (err){
        next(err);
    }
}

async function findFavoriteProjectsForUser(req, res, next){
    try {
        const {id, index, limit} = req.params;
        console.log(id, index, limit);
        const result = await selectFavoriteProjectsForUser(id, index, limit);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

async function countNumberFavoriteProjectsForUser(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await countFavoriteProjectsForUser(id);
        return await res.status(200).json({result: result.rows[0]})
    } catch (err){
        next(err);
    }
}

async function findThreeMoreTimesRiddenProjects(req, res, next){
    try {
        const {id, index, limit} = req.params;
        console.log(id, index, limit);
        const result = await selectThreeMoreTimesRiddenProjects(id, index, limit);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

async function countNumberThreeMoreTimesRiddenProjects(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await countThreeMoreTimesRiddenProjects(id);
        return await res.status(200).json({result: result.rows[0]})
    } catch (err){
        next(err);
    }
}

async function findProjectById(req, res, next){
    try {
        const {id} = req.params;
        console.log(id);
        const result = await selectProjectById(id);
        return await res.status(200).json({result: result.rows[0]})
    } catch (err){
        next(err);
    }
}

async function findProjectsByKeywords(req, res, next){
    try {
        const {id, keywords, index} = req.params;
        console.log(id, keywords, index);
        const negObjArr = await selectNegativeKeys(id);
        const negArr = [];
        for (const obj of negObjArr.rows) {
            negArr.push(+obj.id);
        }
        console.log(negArr)
        const result = await selectProjectsByKeywords(id, keywords, negArr, index);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

async function markProjectAsVoted(req, res, next){
    try {
        const {user_id, project_id, value} = req.body;
        console.log(user_id, project_id, value);

        const pair = await selectProjectUserPair(user_id, project_id);
        console.log('markProjectAsFavorite', pair.rows)
        if (pair.rows.length > 0){
            if (value > 0) {
                if (pair.rows[0].is_liked !== 1) {
                    await updateRatingValueForUserProject(user_id, project_id, value*20, value);
                    await updateRatingValueForProject(user_id, project_id, value);
                    return await res.status(200).json({voted: true})
                } else {
                    return await res.status(200).json({voted: false})
                }
            } else {
                if (pair.rows[0].is_liked !== -1) {
                    await updateRatingValueForUserProject(user_id, project_id, value*20, value);
                    await updateRatingValueForProject(user_id, project_id, value);
                    return await res.status(200).json({voted: true})
                } else {
                    return await res.status(200).json({voted: false})
                }
            }
        } else {
            await insertRatingValueForUserProject(user_id, project_id, value*20, value);
            await updateRatingValueForProject(user_id, project_id, value);
            return await res.status(200).json({voted: true})
        }

    } catch (err){
        next(err);
    }
}

async function incrementUserProjectRatingAfterView(req, res, next){
    try {
        const {user_id, project_id, value} = req.body;
        console.log(user_id, project_id, value);

        const pair = await selectProjectUserPair(user_id, project_id);
        console.log('incrementUserProjectRatingAfterView', pair.rows)
        if (pair.rows.length > 0){
            await updateRatingValueForUserProject(user_id, project_id, value);
        } else {
            await insertRatingValueForUserProject(user_id, project_id, value);
        }
        await updateRiddenForUserProject(user_id, project_id);
        return await res.status(200).json(`User ${user_id} viewed project ${project_id}`)
    } catch (err){
        next(err);
    }
}

async function findPopularProjects(req, res, next){
    try {
        const result = await selectProjectsWithRating(1);
        return await res.status(200).json({result: result.rows})
    } catch (err){
        next(err);
    }
}

module.exports = {
    countNumberFavoriteProjectsForUser,
    countNumberProjectsByAuthor,
    countNumberProjectsByOneKeyword,
    countNumberProjectsByTitle,
    countNumberThreeMoreTimesRiddenProjects,
    findFavoriteProjectsForUser,
    findPopularProjects,
    findProjectById,
    findProjectsByAuthor,
    findProjectsByKeywords,
    findProjectsByOneKeyword,
    findProjectsByTitle,
    findThreeMoreTimesRiddenProjects,
    incrementUserProjectRatingAfterView,
    markProjectAsVoted,
}