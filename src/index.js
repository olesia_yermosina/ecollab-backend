const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

const { usersRouter } = require('./routers/usersRouter');
const { projectsRouter } = require('./routers/projectsRouter');
const { keywordsRouter } = require('./routers/keywordsRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/recommendations/users', usersRouter);
app.use('/api/recommendations/projects', projectsRouter);
app.use('/api/recommendations/keywords', keywordsRouter);

const start = async () => {
    try {
        app.listen(8082);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
};

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
    res.status(400).send({ message: err.message });
}


