const express = require('express');

const router = express.Router();
const {
    markProjectAsVoted,
    findProjectById,
    findProjectsByKeywords,
    findProjectsByTitle,
    findPopularProjects,
    findProjectsByAuthor,
    countNumberProjectsByAuthor,
    countNumberProjectsByTitle,
    countNumberProjectsByOneKeyword,
    findProjectsByOneKeyword,
    findFavoriteProjectsForUser,
    countNumberFavoriteProjectsForUser,
    incrementUserProjectRatingAfterView,
    findThreeMoreTimesRiddenProjects,
    countNumberThreeMoreTimesRiddenProjects,
} = require('../controllers/projectsController');

router.get('/view/:id', findProjectById);

router.get('/popular', findPopularProjects);

router.get('/recommend/:id/:keywords/:index', findProjectsByKeywords);

router.get('/author/:id/:index', findProjectsByAuthor);

router.get('/count/author/:id', countNumberProjectsByAuthor);

router.get('/favorite/:id/:index/:limit', findFavoriteProjectsForUser);

router.get('/count/favorite/:id', countNumberFavoriteProjectsForUser);

router.get('/ridden/:id/:index/:limit', findThreeMoreTimesRiddenProjects);

router.get('/count/ridden/:id', countNumberThreeMoreTimesRiddenProjects);

router.get('/keyword/:id/:index', findProjectsByOneKeyword);

router.get('/count/keyword/:id', countNumberProjectsByOneKeyword);

router.get('/count/:str', countNumberProjectsByTitle);

router.get('/:str/:index', findProjectsByTitle);

router.put('/like', markProjectAsVoted);

router.put('/read', incrementUserProjectRatingAfterView);

module.exports = {
    projectsRouter: router,
};