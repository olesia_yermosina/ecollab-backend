const express = require('express');

const router = express.Router();

const {
    findKeywordByID,
    changeKeywordValueForUser,
    findInterestedKeywords,
    findProjectKeywords,
} = require('../controllers/keywordsController');


router.get('/interests/:id', findInterestedKeywords);

router.get('/project/:id', findProjectKeywords);

router.get('/:id', findKeywordByID);

router.put('/', changeKeywordValueForUser);

module.exports = {
    keywordsRouter: router,
}
