const express = require('express');

const router = express.Router();
const { getUsersByKeywords } = require('../controllers/usersController');

router.get('/recommend/:id/:keywords', getUsersByKeywords);

module.exports = {
    usersRouter: router,
};
