const db = require('../db');

const selectKeywordById = async (keywordID) => await db.query(
    `SELECT name FROM public.keywords WHERE id=${keywordID};`
);

const updateKeywordValueForUser = async (userID, keywordId, value) => await db.query(
    'UPDATE public.user_keyword_rating SET value=value+$1 WHERE user_id=$2 AND keyword_id=$3;',
    [value, userID, keywordId]);

const insertKeywordValueForUser = async (userID, keywordId, value) => await db.query(
    'INSERT INTO public.user_keyword_rating(user_id, keyword_id, value) VALUES ($1, $2, $3);',
    [userID, keywordId, value]);

const selectKeywordUserPair = async (userID, keywordId) => await db.query(
    `SELECT * FROM public.user_keyword_rating 
        WHERE user_id = ${userID} AND keyword_id = ${keywordId}`
);

const selectInterestedKeys = async (userID) => await db.query(
    `SELECT value, us.name as user, ke.name as keyword, ke.id as ID 
        FROM public.user_keyword_rating 
        INNER JOIN public.users as us 
        ON public.user_keyword_rating.user_id = us.id 
        LEFT JOIN public.keywords as ke 
        ON public.user_keyword_rating.keyword_id = ke.id 
        WHERE public.user_keyword_rating.user_id = ${userID} 
        AND public.user_keyword_rating.value > 0 
        ORDER BY user_keyword_rating.value DESC 
        LIMIT 12;`
);

const selectNegativeKeys = async (userID) => await db.query(
    `SELECT ke.id as ID 
        FROM public.user_keyword_rating 
        INNER JOIN public.users as us 
        ON public.user_keyword_rating.user_id = us.id 
        LEFT JOIN public.keywords as ke 
        ON public.user_keyword_rating.keyword_id = ke.id 
        WHERE public.user_keyword_rating.user_id = ${userID} 
        AND public.user_keyword_rating.value <= 0;`
);

const selectUsedKeywords = async (userId) => await db.query(
    `SELECT keywords 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        WHERE uw.user_id = ${userId};`
);

const selectWorkKeywords = async (projectId) => await db.query(
    `SELECT ke.id, ke.name 
        FROM public.works 
        INNER JOIN public.keywords_works as kw ON public.works.id = kw.work_id 
        LEFT JOIN public.keywords as ke ON kw.keyword_id = ke.id 
        WHERE public.works.id=${projectId};`
);

module.exports = {
    insertKeywordValueForUser,
    selectInterestedKeys,
    selectNegativeKeys,
    selectKeywordById,
    selectKeywordUserPair,
    selectUsedKeywords,
    updateKeywordValueForUser,
    selectWorkKeywords,
};