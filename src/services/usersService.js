const db = require('../db')

const selectUsersByKeywords = async (userID, userKeywords, userNegKeywords) => await db.query(
    `SELECT DISTINCT public.users.id, public.users.name 
        FROM public.users 
        INNER JOIN public.users_works as uw ON public.users.id = uw.user_id 
        AND uw.context=0 
        LEFT JOIN public.works as pr ON uw.work_id = pr.id 
        WHERE pr.keywords && Array[${userKeywords}]::bigint[] 
        AND NOT pr.keywords && Array[${userNegKeywords}]::bigint[]
        AND NOT public.users.id = ${userID} 
        LIMIT 10;`
);

module.exports = {
    selectUsersByKeywords,
}