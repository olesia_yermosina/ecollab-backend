const db = require('../db')

const selectProjectsByTitle = async (str, startIndex) => await db.query(
    `SELECT public.works.id, public.works.publication_date as date, public.works.title, us.name AS author, public.works.keywords, public.works.rating 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE LOWER(public.works.title) LIKE LOWER('%${str}%') 
        AND uw.context=0
        ORDER BY rating DESC 
        LIMIT 10 
        OFFSET ${startIndex};`);

const countProjectsByTitle = async (str) => await db.query(
    `SELECT COUNT(*) 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE LOWER(public.works.title) LIKE LOWER('%${str}%') 
        AND uw.context=0;`
);

const selectProjectsByAuthor = async (authorID, startIndex) => await db.query(
    `SELECT public.works.id, title, us.name as author, publication_date as date, tt.name as type, public.works.keywords, rating 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        LEFT JOIN public.types as tt ON public.works.type_id = tt.id 
        WHERE uw.user_id=${authorID} 
        AND uw.context = 0 
        LIMIT 10 
        OFFSET ${startIndex};`
);

const countProjectsByAuthor = async (authorID) => await db.query(
    `SELECT COUNT(*) 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        LEFT JOIN public.types as tt ON public.works.type_id = tt.id 
        WHERE uw.user_id=${authorID} 
        AND uw.context = 0;`
);

const selectProjectById = async (projectID) => await db.query(
    `SELECT public.works.id, title, us.name as author, cus.name as contributor, publication_date as date, language, tt.name as type, pages, file_url, keywords, rating 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id AND uw.context=0 
        LEFT JOIN public.users_works as cw ON public.works.id = cw.work_id AND cw.context=1 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        LEFT JOIN public.users as cus ON cw.user_id = cus.id 
        LEFT JOIN public.types as tt ON public.works.type_id = tt.id 
        WHERE public.works.id = ${projectID};`
);

const selectProjectsByKeywords = async (userID, userKeywords, userNegativeKeywords, index) => await db.query(
    `SELECT public.works.id, title, us.name as author, pages, keywords, publication_date, rating 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE keywords && Array[${userKeywords}]::bigint[] 
        AND NOT keywords && Array[${userNegativeKeywords}]::bigint[]
        AND rating > -1 
        AND NOT us.id = ${userID} 
        AND uw.context = 0 
        AND public.works.id not in (SELECT work_id FROM public.user_work_rating WHERE user_id = 5893 AND is_liked = -1) 
        ORDER BY rating DESC 
        LIMIT 50 
        OFFSET ${index};`
);

const selectProjectsByOneKeyword = async (keywordId, index) => await db.query(
    `SELECT public.works.id, title, us.name as author, pages, keywords, publication_date as date, rating 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE keywords && Array[${keywordId}]::bigint[] 
        AND uw.context = 0 
        ORDER BY rating DESC 
        LIMIT 10 
        OFFSET ${index};`
);

const countProjectsByOneKeyword = async (keywordId) => await db.query(
    `SELECT COUNT(*) 
        FROM public.works 
        INNER JOIN public.users_works as uw ON public.works.id = uw.work_id 
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE keywords && Array[${keywordId}]::bigint[] 
        AND uw.context = 0;`
);

const updateRatingValueForProject = async (userID, projectID, value) => await db.query(
    `UPDATE public.works SET rating=rating+${value} WHERE id=${projectID};`);

const updateRatingValueForUserProject = async (userID, projectID, value, is_liked) => await db.query(
    'UPDATE public.user_work_rating SET rating=rating+$1, is_liked=$4 WHERE user_id=$2 AND work_id=$3;',
    [value, userID, projectID, is_liked]);

const insertRatingValueForUserProject = async (userID, projectID, value, is_liked) => await db.query(
    'INSERT INTO public.user_work_rating(user_id, work_id, rating, is_liked) VALUES ($1, $2, $3, $4);',
    [userID, projectID, value, is_liked]);

const updateRiddenForUserProject = async (userID, projectID) => await db.query(
    'UPDATE public.user_work_rating SET times_read=times_read+1 WHERE user_id=$1 AND work_id=$2;',
    [userID, projectID]);

const selectProjectUserPair = async (userID, projectID) => await db.query(
    `SELECT * FROM public.user_work_rating 
        WHERE user_id = ${userID} AND work_id = ${projectID}`
);

const selectProjectsWithRating = async (rating=1) => await db.query(
    `SELECT public.works.id, title, publication_date, keywords, rating 
        FROM public.works 
        WHERE rating >= ${rating} 
        ORDER BY rating DESC 
        LIMIT 5;`
);

const selectFavoriteProjectsForUser = async (userId, index, limit) => await db.query(
    `SELECT public.user_work_rating.work_id as id, pr.title, us.name as author, pr.publication_date as date, pr.keywords, pr.rating 
        FROM public.user_work_rating 
        INNER JOIN public.works as pr ON public.user_work_rating.work_id = pr.id 
        LEFT JOIN public.users_works as uw ON pr.id = uw.work_id
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE public.user_work_rating.user_id = ${userId} 
        AND public.user_work_rating.is_liked > 0 
        AND uw.context = 0
        ORDER BY public.user_work_rating.rating DESC 
        OFFSET ${index} 
        LIMIT ${limit};`
);

const countFavoriteProjectsForUser = async (userId) => await db.query(
    `SELECT COUNT(*) 
        FROM public.user_work_rating 
        INNER JOIN public.works as pr ON public.user_work_rating.work_id = pr.id 
        LEFT JOIN public.users_works as uw ON pr.id = uw.work_id
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE public.user_work_rating.user_id = ${userId} 
        AND public.user_work_rating.is_liked > 0
        AND uw.context = 0;`
);

const selectThreeMoreTimesRiddenProjects = async (userId, index, limit) => await db.query(
    `SELECT public.user_work_rating.work_id as id, pr.title, us.name as author, pr.publication_date as date, pr.keywords, pr.rating 
        FROM public.user_work_rating 
        INNER JOIN public.works as pr ON public.user_work_rating.work_id = pr.id 
        LEFT JOIN public.users_works as uw ON pr.id = uw.work_id
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE public.user_work_rating.user_id = ${userId} 
        AND public.user_work_rating.times_read >= 3 
        AND uw.context = 0
        ORDER BY public.user_work_rating.times_read DESC 
        OFFSET ${index} 
        LIMIT ${limit};`
);

const countThreeMoreTimesRiddenProjects = async (userId) => await db.query(
    `SELECT COUNT(*) 
        FROM public.user_work_rating 
        INNER JOIN public.works as pr ON public.user_work_rating.work_id = pr.id 
        LEFT JOIN public.users_works as uw ON pr.id = uw.work_id
        LEFT JOIN public.users as us ON uw.user_id = us.id 
        WHERE public.user_work_rating.user_id = ${userId} 
        AND public.user_work_rating.times_read >= 3
        AND uw.context = 0;`
);

module.exports = {
    countFavoriteProjectsForUser,
    countProjectsByAuthor,
    countProjectsByOneKeyword,
    countProjectsByTitle,
    countThreeMoreTimesRiddenProjects,
    insertRatingValueForUserProject,
    selectFavoriteProjectsForUser,
    selectProjectById,
    selectProjectsByAuthor,
    selectProjectsByOneKeyword,
    selectProjectsByKeywords,
    selectProjectsByTitle,
    selectProjectUserPair,
    selectProjectsWithRating,
    selectThreeMoreTimesRiddenProjects,
    updateRatingValueForProject,
    updateRatingValueForUserProject,
    updateRiddenForUserProject,
}
